import random
import timeit
from functools import reduce

import pytest


def combine_orders_with_while(requests, n_max):
    if n_max == 0:
        return float("inf")

    requests.sort(reverse=True)
    envoy_count = 0
    while requests:
        envoy_loads = 0
        envoy_capacity = n_max
        for request in requests:
            if request <= envoy_capacity:
                envoy_capacity -= request
                requests.remove(request)
                envoy_loads +=1
                if envoy_capacity == 0 or envoy_loads == 2:
                    break
                if len(requests) == 1 and requests[0] <= envoy_capacity:
                    requests.remove(requests[0])

        envoy_count += 1

    return envoy_count


def combine_orders_with_while_double_sorted_lists(requests, n_max):
    if n_max == 0:
        return float("inf")

    requests.sort(reverse=True)
    asc_requests = sorted(requests)
    envoy_count = 0

    while requests:
        envoy_capacity = n_max

        current_request = requests[0]
        envoy_capacity -= current_request
        requests.remove(current_request)

        if envoy_capacity != 0 and len(requests) > 0:
            found_matching_envoy = False
            for request in asc_requests:
                if request == envoy_capacity:
                    asc_requests.remove(request)
                    found_matching_envoy = True
                    break

                if request > envoy_capacity and asc_requests.index(request) > 0:
                    previous_request = asc_requests[asc_requests.index(request) - 1]
                    if previous_request <= envoy_capacity:
                        asc_requests.remove(previous_request)
                        found_matching_envoy = True
                        break

            if (
                not found_matching_envoy
                and envoy_capacity >= requests[0]
                and len(requests) > 0
            ):
                next_request = requests[0]
                requests.remove(next_request)

        envoy_count += 1

    return envoy_count


def combine_orders_with_reduce(requests, n_max):
    if n_max == 0:
        return float("inf")

    requests.sort(reverse=True)

    def reducer(envoys, request):
        remaining_capacity = n_max - request
        matching_request_index = None

        for i, r in enumerate(requests):
            if r <= remaining_capacity:
                matching_request_index = i
                break

        if matching_request_index is not None:
            requests.pop(matching_request_index)

        return envoys + 1

    envoy_count = reduce(reducer, requests, 0)

    return envoy_count


@pytest.fixture(autouse=True)
def sample_data():
    requests = [random.randint(1, 200) for _ in range(100)]
    n_max = 200
    return requests, n_max


def execution_time(func, sample_data):
    requests, n_max = sample_data

    start_time = timeit.default_timer()
    func(requests, n_max)
    end_time = timeit.default_timer()
    execution_time = end_time - start_time

    print(f"Function: {func.__name__}\tExecution Time: {execution_time:.6f} seconds")


def test_performance_combine_orders_with_while(sample_data):
    requests, n_max = sample_data

    execution_time(combine_orders_with_while, (requests, n_max))


def test_performance_combine_orders_with_while_double_sorted_lists(sample_data):
    requests, n_max = sample_data

    execution_time(combine_orders_with_while_double_sorted_lists, (requests, n_max))


def test_performance_combine_orders_with_reduce(sample_data):
    requests, n_max = sample_data

    execution_time(combine_orders_with_reduce, (requests, n_max))
