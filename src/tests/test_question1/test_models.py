from question1.models import Contracts

from .factories import ContractFactory, RenegotiationFactory


def test_get_top_N_open_contracts():
    contract1 = ContractFactory.build(id=1, debt=1000.0)
    contract2 = ContractFactory.build(id=2, debt=2000.0)
    contract3 = ContractFactory.build(id=3, debt=3000.0)
    contract4 = ContractFactory.build(id=4, debt=4000.0)
    contract5 = ContractFactory.build(id=5, debt=5000.0)
    renegotiations_contracts = [
        RenegotiationFactory.build(contract_id=2),
        RenegotiationFactory.build(contract_id=4),
    ]
    renegotiations_contracts = list(
        map(lambda renegotiation: renegotiation.contract_id, renegotiations_contracts)
    )
    open_contracts = [contract1, contract2, contract3, contract4, contract5]

    top_contracts = Contracts.get_top_N_open_contracts(
        open_contracts, renegotiations_contracts, 2
    )

    expected_top_contracts = [contract5, contract3]
    assert top_contracts == expected_top_contracts


def test_get_top_N_open_contracts_with_empty_renegotiations():
    contract1 = ContractFactory.build(id=1, debt=1000.0)
    contract2 = ContractFactory.build(id=2, debt=2000.0)
    contract3 = ContractFactory.build(id=3, debt=3000.0)
    contract4 = ContractFactory.build(id=4, debt=4000.0)
    contract5 = ContractFactory.build(id=5, debt=5000.0)
    renegotiations_contracts = []
    renegotiations_contracts = list(
        map(lambda renegotiation: renegotiation.contract_id, renegotiations_contracts)
    )
    open_contracts = [contract1, contract2, contract3, contract4, contract5]

    top_contracts = Contracts.get_top_N_open_contracts(
        open_contracts, renegotiations_contracts, 2
    )

    expected_top_contracts = [contract5, contract4]
    assert top_contracts == expected_top_contracts


def test_get_top_N_open_contracts_with_empty_open_contracts():
    renegotiations_contracts = []
    renegotiations_contracts = list(
        map(lambda renegotiation: renegotiation.contract_id, renegotiations_contracts)
    )

    top_contracts = Contracts.get_top_N_open_contracts([], renegotiations_contracts, 2)

    expected_top_contracts = []
    assert top_contracts == expected_top_contracts
