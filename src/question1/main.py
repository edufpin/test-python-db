import random

from fastapi import Depends, FastAPI
from fastapi.responses import RedirectResponse

from .database import Session
from .models import Contract, Contracts, Renegotiation

app = FastAPI()


async def get_db():
    db_session = Session()
    try:
        yield db_session
    finally:
        db_session.close()


@app.get("/")
async def redirect_to_docs():
    return RedirectResponse(url="/docs")


@app.post("/create-samples/")
def create_samples(size: int = 10, db: Session = Depends(get_db)):
    for _ in range(size):
        contract = Contract(debt=round(random.uniform(1000.0, 5000.0), 2))
        if random.random() <= 0.2:
            renegotiation = Renegotiation(contract=contract)
            db.add(renegotiation)
        db.add(contract)
    db.commit()

    return {"message": f"Created {size} samples."}


@app.get("/top-open-contracts/")
def get_top_open_contracts(size: int = 1, db: Session = Depends(get_db)):
    open_contracts = db.query(Contract).all()
    renegotiations = db.query(Renegotiation).all()
    renegotiations_contracts = list(
        map(lambda renegotiation: renegotiation.contract_id, renegotiations)
    )

    open_contracts = Contracts.get_top_N_open_contracts(
        open_contracts, renegotiations_contracts, size
    )

    return open_contracts
