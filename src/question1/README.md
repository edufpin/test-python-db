## Requirements

- Python 3.8.x
- Poetry

## Instalation

Go to project directory(src) and install dependencies:
```
poetry install
```


## How to Execute

Activate poetry:
```
poetry shell
```

Run the API:
```
uvicorn question1.main:app --reload
```

Run projects tests:
```
poetry run pytest -sx
```

Also, is possible to run tests through Makefile:
```
make test
```

Swagger API:
```
http://127.0.0.1:8000/docs/
```

Functions can be executed through Swagger interface.

## Logic decisions

FastAPI was used to integrate the function of project, you can access Swagger to run it in as below:
```
/create-samples/ for sample creating.
/top-open-contracts/ return top open contracts.
```
Create samples will create a X number of samples, each one with 20% change of having a renegotiation, generating a random samples.
SQLite was used as database only to facilitate testing.

A file with processing time tests was created in **tests/test_question1/test_processing_time.py**, showing results as below with a sample of 1000000 open contracts:

```
Function: get_top_N_open_contracts_with_for_and_sorted Execution Time: 4.068220 seconds
Function: get_top_N_open_contracts_with_for_and_heapq Execution Time: 4.072625 seconds
Function: get_top_N_open_contracts_with_comprehension_and_sorted Execution Time: 4.020592 seconds
Function: get_top_N_open_contracts_with_comprehension_and_heapq Execution Time: 3.997046 seconds
Function: get_top_N_open_contracts_with_lambda_filter_and_sorted Execution Time: 4.188301 seconds
Function: get_top_N_open_contracts_with_lambda_filter_and_heapq Execution Time: 4.214114 seconds
```

And with a sample of 100 open contracts:
```
Function: get_top_N_open_contracts_with_for_and_sorted Execution Time: 0.000479 seconds
Function: get_top_N_open_contracts_with_for_and_heapq Execution Time: 0.000387 seconds
Function: get_top_N_open_contracts_with_comprehension_and_sorted Execution Time: 0.000376 seconds
Function: get_top_N_open_contracts_with_lambda_filter_and_sorted Execution Time: 0.000411 seconds
Function: get_top_N_open_contracts_with_comprehension_and_heapq Execution Time: 0.000440 seconds
Function: get_top_N_open_contracts_with_lambda_filter_and_heapq Execution Time: 0.000478 seconds
```

Based on this, for this project a function with list comprehension and heapq was used, since it will perfome well on both scenarios and better with big dat samples.
Anyway list comprehension and sorted would be a good option too.
```
class Contracts:
    def get_top_N_open_contracts(open_contracts, renegotiated_contracts, top_n):
        contracts_without_renegotiation = [contract for contract in open_contracts if contract.id not in renegotiated_contracts]
        top_contracts = heapq.nlargest(top_n, contracts_without_renegotiation, key=lambda contract: contract.debt)
        return top_contracts
```
*Performance can vary based on sample size, other functions can perfome similar with fewer samples.
