from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from question1.database import Base
from question1.main import app, get_db

from .factories import ContractFactory, RenegotiationFactory

SQLALCHEMY_DATABASE_URL = "sqlite:///./test.db"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


Base.metadata.create_all(bind=engine)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)


def test_create_samples():
    response = client.post("/create-samples/")
    assert response.status_code == 200


def test_get_top_open_contracts():
    ContractFactory(id=1, debt=1000.0)
    ContractFactory(id=2, debt=2000.0)
    ContractFactory(id=3, debt=3000.0)
    ContractFactory(id=4, debt=4000.0)
    ContractFactory(id=5, debt=5000.0)
    RenegotiationFactory(contract_id=2)
    RenegotiationFactory(contract_id=4)

    response = client.get("/top-open-contracts/?size=3")
    assert response.status_code == 200
    assert len(response.json()) == 3
