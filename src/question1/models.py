import heapq

from sqlalchemy import Column, Float, ForeignKey, Integer
from sqlalchemy.orm import relationship

from .database import Base, engine


class Contract(Base):
    __tablename__ = "contracts"

    id = Column(Integer, primary_key=True, index=True)
    debt = Column(Float)

    renegotiations = relationship("Renegotiation", back_populates="contract")


class Renegotiation(Base):
    __tablename__ = "renegotiations"

    id = Column(Integer, primary_key=True, index=True)
    contract_id = Column(Integer, ForeignKey("contracts.id"))

    contract = relationship("Contract", back_populates="renegotiations")


class Contracts:
    def get_top_N_open_contracts(open_contracts, renegotiated_contracts, top_n):
        contracts_without_renegotiation = [
            contract
            for contract in open_contracts
            if contract.id not in renegotiated_contracts
        ]
        top_contracts = heapq.nlargest(
            top_n, contracts_without_renegotiation, key=lambda contract: contract.debt
        )
        return top_contracts


Base.metadata.create_all(engine)
