import heapq


class Contract:
    def __init__(self, id, debt):
        self.id = id
        self.debt = debt

    def __str__(self):
        return "id={}, debt={}".format(self.id, self.debt)


class Contracts:
    def get_top_N_open_contracts(self, open_contracts, renegotiated_contracts, top_n):
        contracts_without_renegotiation = [
            contract
            for contract in open_contracts
            if contract.id not in renegotiated_contracts
        ]
        top_contracts = heapq.nlargest(
            top_n, contracts_without_renegotiation, key=lambda contract: contract.debt
        )
        return top_contracts
