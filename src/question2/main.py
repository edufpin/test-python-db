from typing import List

from fastapi import Body, FastAPI
from fastapi.responses import RedirectResponse

from .models import Orders

app = FastAPI()


@app.get("/")
async def redirect_to_docs():
    return RedirectResponse(url="/docs")


@app.post("/envoy-requsitions/")
def get_envoy_requisitions(
    requests: List[int] = Body([1], description="List of integer requests"),
    n_max: int = Body(10, description="Maximum value an envoy can carry"),
):
    envoys_needed = Orders.combine_orders(requests, n_max)
    return {"envoys_needed": envoys_needed}
