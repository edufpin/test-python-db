from factory import Faker, SubFactory
from factory.alchemy import SQLAlchemyModelFactory

from question1.database import Session
from question1.models import Contract, Renegotiation


class ContractFactory(SQLAlchemyModelFactory):
    class Meta:
        model = Contract
        sqlalchemy_session = Session

    debt = Faker("pydecimal", left_digits=5, right_digits=2, positive=True)


class RenegotiationFactory(SQLAlchemyModelFactory):
    class Meta:
        model = Renegotiation
        sqlalchemy_session = Session

    contract = SubFactory(ContractFactory)
