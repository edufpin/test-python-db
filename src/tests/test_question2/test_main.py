from fastapi.testclient import TestClient

from question2.main import app

client = TestClient(app)


def test_get_envoy_requisitions():
    payload = {"requests": [4, 7, 3], "n_max": 10}
    response = client.post("/envoy-requsitions/", json=payload)
    assert response.status_code == 200
    assert response.json() == {"envoys_needed": 2}
