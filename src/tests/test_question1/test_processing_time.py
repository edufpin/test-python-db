import heapq
import random
import timeit

import pytest

from .factories import ContractFactory, RenegotiationFactory


def get_top_N_open_contracts_with_comprehension_and_sorted(
    open_contracts, renegotiated_contracts, top_n
):
    contracts_without_renegotiation = [
        contract
        for contract in open_contracts
        if contract.id not in renegotiated_contracts
    ]
    sorted_contracts = sorted(
        contracts_without_renegotiation,
        key=lambda contract: contract.debt,
        reverse=True,
    )
    return sorted_contracts[:top_n]


def get_top_N_open_contracts_with_lambda_filter_and_sorted(
    open_contracts, renegotiated_contracts, top_n
):
    contracts_without_renegotiation = list(
        filter(
            lambda contract: contract.id not in renegotiated_contracts, open_contracts
        )
    )
    sorted_contracts = sorted(
        contracts_without_renegotiation,
        key=lambda contract: contract.debt,
        reverse=True,
    )
    return sorted_contracts[:top_n]


def get_top_N_open_contracts_with_comprehension_and_heapq(
    open_contracts, renegotiated_contracts, top_n
):
    contracts_without_renegotiation = [
        contract
        for contract in open_contracts
        if contract.id not in renegotiated_contracts
    ]
    top_contracts = heapq.nlargest(
        top_n, contracts_without_renegotiation, key=lambda contract: contract.debt
    )
    return top_contracts


def get_top_N_open_contracts_with_lambda_filter_and_heapq(
    open_contracts, renegotiated_contracts, top_n
):
    contracts_without_renegotiation = list(
        filter(
            lambda contract: contract.id not in renegotiated_contracts, open_contracts
        )
    )
    top_contracts = heapq.nlargest(
        top_n, contracts_without_renegotiation, key=lambda contract: contract.debt
    )
    return top_contracts


def get_top_N_open_contracts_with_for_and_sorted(
    open_contracts, renegotiated_contracts, top_n
):
    contracts_without_renegotiation = []
    for contract in open_contracts:
        if contract.id not in renegotiated_contracts:
            contracts_without_renegotiation.append(contract)
    sorted_contracts = sorted(
        contracts_without_renegotiation,
        key=lambda contract: contract.debt,
        reverse=True,
    )
    return sorted_contracts[:top_n]


def get_top_N_open_contracts_with_for_and_heapq(
    open_contracts, renegotiated_contracts, top_n
):
    contracts_without_renegotiation = []
    for contract in open_contracts:
        if contract.id not in renegotiated_contracts:
            contracts_without_renegotiation.append(contract)
    top_contracts = heapq.nlargest(
        top_n, contracts_without_renegotiation, key=lambda contract: contract.debt
    )
    return top_contracts


@pytest.fixture(scope="module")
def sample_data():
    contracts = []
    renegotiations = []
    for _ in range(100):
        contract = ContractFactory(debt=random.uniform(1000.0, 5000.0))
        contracts.append(contract)
        if random.random() <= 0.2:
            renegotiation = RenegotiationFactory(contract=contract.id)
            renegotiations.append(renegotiation.contract_id)
    return contracts, renegotiations


def execution_time(func, sample_data):
    contracts, renegotiations = sample_data

    start_time = timeit.default_timer()
    func(contracts, renegotiations, 100)
    end_time = timeit.default_timer()
    execution_time = end_time - start_time

    print(f"Function: {func.__name__}\tExecution Time: {execution_time:.6f} seconds")


def test_performance(sample_data):
    implementations = [
        get_top_N_open_contracts_with_for_and_sorted,
        get_top_N_open_contracts_with_for_and_heapq,
        get_top_N_open_contracts_with_comprehension_and_sorted,
        get_top_N_open_contracts_with_lambda_filter_and_sorted,
        get_top_N_open_contracts_with_comprehension_and_heapq,
        get_top_N_open_contracts_with_lambda_filter_and_heapq,
    ]

    for implementation in implementations:
        execution_time(implementation, sample_data)
