class Orders:
    def combine_orders(self, requests, n_max):
        if n_max == 0:
            return float("inf")

        requests.sort(reverse=True)
        envoy_count = 0
        while requests:
            envoy_loads = 0
            envoy_capacity = n_max
            for request in requests:
                if request <= envoy_capacity:
                    envoy_capacity -= request
                    requests.remove(request)
                    envoy_loads +=1
                    if envoy_capacity == 0 or envoy_loads == 2:
                        break
                    if len(requests) == 1 and requests[0] <= envoy_capacity:
                        requests.remove(requests[0])

            envoy_count += 1

        return envoy_count
