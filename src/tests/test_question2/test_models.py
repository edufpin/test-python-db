import pytest

from question2.models import Orders


@pytest.mark.parametrize(
    "requests, n_max, expected_envoys_needed",
    [
        ([1, 10, 4, 7, 5, 2, 3, 9, 8, 6], 10, 6),
        ([4, 5, 1, 2, 5], 5, 4),
        ([3, 3, 3, 3, 3], 5, 5),
        ([3, 3, 3, 3, 3], 10, 3),
        ([3, 3, 3, 3, 3, 3], 10, 3),
        ([2, 2, 2, 2, 2, 2, 2], 10, 4),
        ([10, 7, 4], 10, 3),
        ([1, 7, 3], 10, 2),
        ([1, 7, 4], 10, 2),
    ],
)
def test_combine_orders(requests, n_max, expected_envoys_needed):
    envoys_needed = Orders.combine_orders(requests, n_max)
    assert envoys_needed == expected_envoys_needed


def test_combine_orders_single_request():
    requests = [5]
    n_max = 10
    expected_envoys_needed = 1

    envoys_needed = Orders.combine_orders(requests, n_max)
    assert envoys_needed == expected_envoys_needed


def test_combine_orders_zero_requests():
    requests = []
    n_max = 10
    expected_envoys_needed = 0

    envoys_needed = Orders.combine_orders(requests, n_max)
    assert envoys_needed == expected_envoys_needed


def test_combine_orders_zero_n_max():
    requests = [10, 20, 30]
    n_max = 0
    expected_envoys_needed = float("inf")

    envoys_needed = Orders.combine_orders(requests, n_max)
    assert envoys_needed == expected_envoys_needed
